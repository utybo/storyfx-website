---
layout: default
title: Home
nav_order: 1
permalink: /
---

# StoryFX
{: .fs-9 .fw-700 }

Create great stories, in Kotlin or with just plain text, right here, right now.
Free and open source.
{: .fs-6 .fw-300 }

![StoryFX homescreen]({{ site.baseurl }}/assets/img/storyfxhome.png)

---

StoryFX is a next-gen story player. It allows you to build fantastic stories
from plain text or Kotlin code, with lots of openings for dynamism and other
visual candy.

You get a lot of options when creating in StoryFX. You can write stories using
the 
[simple (but limited) story.txt]({{ base.url }}{% link docs/storytxt/storytxt.md %})
syntax with plain text...

```
[1]
This is my first node!
{Go to the second one!} 2

[2]
Hey, you found me!
```

...or go all in with a more complex but extremely powerful story format built in
Kotlin, [story.kts]({{ base.url }}{% link docs/storykts/storykts.md%})!

```kotlin
story {
    node(1) {
        body {
            """
            Want me to say hello?
            """
        }
        option { "Yes please!" } goesTo { 2 }
    }

    node(2) body {
        """
        Hello!
        """
    }
}
```

You can even mix and match both to get the best of both worlds!

```kotlin
"""
[1]
This is my first node... How do I go to the second one?

[2]
Wow, you found out how!
""" import {
    inNode(1) {
        option { "Like this!" } goesTo { 2 }
    }
}
```

**This project is a work in progress and is not ready for public release.**
