---
layout: default
title: story.txt integration
parent: story.kts guide
nav_order: 20
---

# story.txt integration
{: .no_toc }

1. TOC
{: toc }

## Importing story.txt stories

There are two ways of importing story.txt stories into story.kts files.

1. Either by encoding the txt story directly in the kts file (embedded)
2. Or by loading an external story.txt file (resource-based)

Importing a story.txt file allows you to modify it and get the best of both
worlds. You are able to write your textual content and simple nodes using
the [story.txt format]({% link docs/storytxt/storytxt.md %}).

### Embedded story.txt

The basic syntax is as follows:

```kotlin
"""
[1]
This is my story
{Amazing!} 2

[2]
ikr
""" import {
    // Story block
}
```

The story block behaves in exactly the same way as a regular story block. The
story is initialized with all of the data provided in the string, and the story
block allows you to fully customize everything.

### Resource-based story.txt import

This method has a few things you need to keep in mind:

* You will need to call `loadResources()` before importing anything
* The engine has to support resources

Let's say that we have two stories organized like this:

```
myfolder
+- resources
|   +- myfile.story.txt
+- amazing.story.kts
```

The syntax is as follows, in the `amazing.story.kts` file:

```kotlin
importTxt(resources["myfile.story.txt"]) {
    // Story block
}
```


## Customizing the imported story

So far, you have seen how to create nodes and modify them through their blocks.

However, the syntax you know always creates a node. Here, the nodes already 
exist, we only want to add some more content to them.

We can access a node and modify its content with the `inNode` function:

```
"""
[1]
This is my first node... Now, how do I go to the second one?

A complete mystery!

[2]
Wow! How did you do that?!
""" import {
    inNode(1) {
        option { "Hm... I wonder..." } goesTo { 2 }
    }
}
```

The `inNode` function retrieves a node that already exists and runs the node
block on it. Here, we are creating an option in this way. `inNode` can also be
used on string IDs. You can do everything you already could do in regular node
blocks.


## Bindings

You can automatically get some values replaced in your file.

The story.txt format does not support variables, but story.kts does. In order
to merge both worlds, you need to define bindings. Bindings simply replace
every instance of a string by a value which is entirely customizable.

```kotlin
var aVariable = 10

"""
[1]
This is %myVariable!
{Go to 2} 2

[2]
Woow!
""" import {
    inNode(1) {
        bind("%myVariable" to { aVariable })
    }
}
```

The `bind` function supports specifying multiple bindings at the same time:

```kotlin
bind("%something" to { "something else" }, "yes" to { "no" }, "maybe" to {"assuredly"})
```

Note that bindings modify the body of nodes. Make sure you apply bindings after all modifications you make to bodies.

You can also use `inAllNodes` to apply a node block to all nodes in a story.
This is very useful for defining story-wide bindings.

```kotlin
"""
[1]
Replace me! Replace me!

[2]
Replace me! Again!

[3]
Replace me! Yes! Replace me!
""" import {
    inAllNodes {
        bind("Replace me!" to {"Replaced!"})
    }
}
```