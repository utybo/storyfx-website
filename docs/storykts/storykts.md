---
layout: default
title: story.kts guide
nav_order: 3
has_children: true
permalink: /docs/storykts/
---

# story.kts format

The `story.kts` format is a Kotlin DSL (domain specific language) that allows
you to write stories using the Kotlin programming language.

While it is not strictly necessary to know how to code using the Kotlin language,
it will be a great help for using the `story.kts` format.

## What is the story.kts format?

The `story.kts` format is simply a Kotlin Script (kts) file that uses a specific
template
[defined and implemented in libstorytree](https://github.com/utybo/StoryFX/blob/master/libstorytree/src/main/kotlin/guru/zoroark/libstorytree/dsl/StoryBuildScript.kt)
(a flexible basis on top of which StoryFX is built) using
[Kotlin's Scripting Support (KEEP 75)](https://github.com/Kotlin/KEEP/issues/75).

It aims to provide a painless and efficient way of writing branching stories

Upsides of using this format include a clean syntax that is actual Kotlin code
and is extremely flexible, while being more maintainable and safer than
home-made recepies. The Kotlin language is massively supported by Google in
Android and is also very suitable for desktop applications. The syntax is
quite a painless one for beginners too, making it appropriate for a story 
building project.

Downsides are that it is very annoying to write many nodes at once, and is not
appropriate for writers who only want simple stories. These writers may enjoy
the old (and discontinued) [BST](https://utybo.github.io/BST) syntax more than
the `story.kts` format.