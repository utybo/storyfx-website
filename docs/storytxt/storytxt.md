---
layout: default
title: story.txt guide
nav_order: 2
permalink: /docs/storytxt/
---

# story.txt guide

The `story.txt` format is the opposite of the `story.kts` syntax. It is very 
limited, but is infinitely easier to use and provides a manageable syntax.

You do not need any particular software to get started. Everything you need to 
know is in this page!

## Defining a story

The rule is that one file = one story with `story.txt`. Thus, you do not have
much to worry about story-wise.

The files can have any name but must end in `.story.txt`. You can use any
editing software to edit the files, from Notepad to Visual Studio Code.

Note that `story.txt` files support comments. Lines that begin with `//` are 
ignored and skipped.

## Defining a node

Every "part" of your story is defined as a node. Each node has different options.

For example, say Brian is in the kitchen. This could be our first node, where we
explain his situation. Then, the options are simply directing to other nodes
which describe what happens when he is in other rooms.

Nodes are defined with three things:

* A header, which defines the node's identifier
* A body, which is the text contained by the node
* Zero or more options, which tell which nodes can be accessed from this node

```
[id]
This is the text of the node

It can span on multiple lines.

{This is an option, followed by the id of the node this option leads to} id
```

Identifiers can be numbers or strings of characters. Two nodes must never share
the same identifier.

An example of a story could be:

```
[1]
Brian is in the kitchen
{Go to the living room} living1
{Look for food} 2

[living1]
Brian is in the living room now
{Sleep in the couch} living2

[living2]
Brian is now sound asleep in the couch.

The end.

[2]
Brian finds an apple and eats it.
```

The story ends when there are no options set for the node that was reached.

## Defining properties

`story.txt` does not offer much room when it comes to customizability.
It does support properties.

Properties are simple settings placed before the very first node in the file.
They have the following syntax.

```
propertyName = Property value
```

There are three properties available:

* `title`: the title of the story
* `author`: the author of the story
* `initialNode`: the node at which the story should start. The default is `1`.

```
title = This is my story
author = Me, myself and I
initialNode = living1
```