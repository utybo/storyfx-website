---
layout: default
title: Running development builds
parent: StoryFX docs
nav_order: 1
---

# Running development builds

If you want to have a look at the latest improvements, you can download and
run the latest development version of StoryFX.

To do so, you will need to install [Git](https://git-scm.com/) and a Java 12
JDK. For our recommendation, check [this page's requirements]({{ site.baseurl
}}{% link docs/storykts/using-template.md %}).

Once all of these are installed, create a new directory, and open this directory
in your favorite terminal. (On Windows, shift right click then select Open in
PowerShell)

Type in the following commands:

```
git clone https://github.com/utybo/StoryFX
cd StoryFX
./gradlew run
```

The last command will take a long time as it needs to download many
dependencies. Once it is done, it should directly open StoryFX.

If you want to update your local copy with the latest changes, use the following
command inside the StoryFX folder:

```
git pull
```

If you want to run the program again, use the following command inside the
StoryFX folder:

```
./gradlew run
```